package zos.builder;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.function.Consumer;

class StreamGobbler implements Runnable {
	private InputStream inputStream;
	private Consumer<String> consumer;
	private boolean finished = false;

	public StreamGobbler(InputStream inputStream, Consumer<String> consumer) {
		this.inputStream = inputStream;
		this.consumer = consumer;
		this.finished = false;
	}

	@Override
	public void run() {
		new BufferedReader(new InputStreamReader(inputStream)).lines().forEach(consumer);
		this.finished = true;

	}

	public boolean isFinished() {
		return finished;
	}
}