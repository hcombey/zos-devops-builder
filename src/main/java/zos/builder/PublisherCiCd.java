package zos.builder;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;

public class PublisherCiCd {
	static String HOST_IP = "192.86.32.153";
	static String HOST_PORT = "10443";
	static String USER = "Z01390";
	static String PASSWORD = "build255";
	static String SECOND_HLQ = "";

	ConsoleOutputCapturer consoleOutputCapturer = new ConsoleOutputCapturer();

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PublisherCiCd publisherCiCd = new PublisherCiCd();
		try {

			System.getenv().forEach((k, v) -> {
				System.out.println(k + ":" + v);
			});
			HOST_IP = System.getenv("HOST_IP");
			HOST_PORT = System.getenv("HOST_PORT");
			USER = System.getenv("USER");
			PASSWORD = System.getenv("PASSWORD");
			SECOND_HLQ = System.getenv("SECOND_HLQ");
			if (SECOND_HLQ != null && SECOND_HLQ.length() == 0)
				SECOND_HLQ = null;
			else
				System.out.println("We are going to had " + SECOND_HLQ + " as second HLQ for all your files");

			System.out.println("Your project contains these files in /home/project :");
			Process process = Runtime.getRuntime().exec("ls /home/project");
			StreamGobbler streamGobbler = new StreamGobbler(process.getInputStream(), System.out::println);
			Executors.newSingleThreadExecutor().submit(streamGobbler);
			int exitCode = process.waitFor();

			/*
			 * process = Runtime.getRuntime().exec("zowe"); streamGobbler = new StreamGobbler(process.getInputStream(), System.out::println);
			 * Executors.newSingleThreadExecutor().submit(streamGobbler);
			 */
			// connexion ZOWE
			publisherCiCd.connexionZowe();

			// Lecture de tous les fichiers en local
			File[] files = new File("/home/project").listFiles();
			// Récupération des datas dans .description
			BufferedReader fdDescription = new BufferedReader(new FileReader("/home/project/.description"));
			ArrayList<FileDescription> fds = new ArrayList<>();
			String str = fdDescription.readLine();
			str = fdDescription.readLine(); // First line is the comment!
			while (str != null) {
				str = str.trim();
				if (str.indexOf(" : ") > 0) {
					String[] tab = str.split(" : ");
					fds.add(new FileDescription(tab[0], tab[1]));
				}
				str = fdDescription.readLine();
			}
			for (File file : files) {
				if (file.getName().startsWith("."))
					continue;
				System.out.println("Traitement de " + file.getName());
				String ds = file.getName();

				if (file.isDirectory()) {
					// Je check qu'il est bien déclaré.
					int idx = fds.indexOf(new FileDescription(ds, ""));
					if (idx == -1)
						throw new Exception("PDS " + ds + " not present in .description!!");
					if (!fds.get(idx).dcb.equals("PDS SOURCE"))
						throw new Exception("PDS " + ds + " is not declared as PDS source in .description!!");
					if (SECOND_HLQ != null) {
						String HLQ = ds.substring(0, ds.indexOf("."));
						String END = ds.substring(ds.indexOf("."));
						ds = HLQ + "." + SECOND_HLQ + END;
					}
					if (!publisherCiCd.datasetExists(ds)) {
						publisherCiCd.executeCommande("zowe zos-files create data-set-classic \"" + ds + "\"");
					}
					publisherCiCd.executeCommande("zowe zos-files upload dir-to-pds \"" + file.getCanonicalPath() + "\" " + ds);
				} else {
					// Je check qu'il est bien déclaré.
					int idx = fds.indexOf(new FileDescription(ds, ""));
					if (idx == -1)
						throw new Exception("DS " + ds + " not present in .description!!");
					String dcb = fds.get(idx).dcb;
					if (SECOND_HLQ != null) {
						String HLQ = ds.substring(0, ds.indexOf("."));
						String END = ds.substring(ds.indexOf("."));
						ds = HLQ + "." + SECOND_HLQ + END;
					}
					if (!publisherCiCd.datasetExists(ds)) {
						publisherCiCd.executeCommande("zowe zos-files create data-set-sequential  \"" + ds + "\" " + dcb);
					}
					publisherCiCd.executeCommande("zowe zos-files upload file-to-data-set \"" + file.getCanonicalPath() + "\" " + ds);
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			publisherCiCd.writeResult("KO");
			System.exit(1);
		}
		publisherCiCd.writeResult("OK");
		System.exit(0);
	}

	private void writeResult(String str) {
		System.out.println("Writing the result : " + str + "...");
		try {
			BufferedWriter myBuff = new BufferedWriter(new FileWriter("/home/project/result"));
			myBuff.write(str);
			myBuff.close();
			System.out.println("Writing the result finished normally");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Writing the result finished has failed!!!");
			e.printStackTrace();
			System.exit(1);
		}

	}

	public void connexionZowe() throws Exception {
		String command = "zowe profiles create zosmf-profile mtm2020 --host " + HOST_IP + " --port " + HOST_PORT + "  --user " + USER + " --password "
				+ PASSWORD + " --reject-unauthorized false";
		executeCommande(command);

		command = "zowe profiles create tso-profile mtm2020 -a fb3";
		executeCommande(command);

		command = "zowe  zos-tso issue command \"status\"";
		List<String> res = executeCommande(command);
		for (String str : res)
			System.out.println("capture = " + str);
	}

	List<String> executeCommande(String command) throws Exception {
		System.out.println("Executing : " + command + "...");
		this.consoleOutputCapturer.start();

		Process process = Runtime.getRuntime().exec(command);
		StreamGobbler streamGobbler = new StreamGobbler(process.getInputStream(), System.out::println);
		Executors.newSingleThreadExecutor().submit(streamGobbler);
		int exitCode = process.waitFor();
		if (exitCode == 0)
			System.out.println("Execution successful");
		else {
			System.err.println("Execution KO : " + exitCode);
			throw new Exception("FAILED on command : " + command);
		}
		String output = this.consoleOutputCapturer.stop();
		String[] tab = output.split("\n");
		return Arrays.asList(tab);
	}

	private boolean datasetExists(String ds) throws Exception {
		System.out.println("Checking if " + ds + " already exists on Z/OS");
		List<String> lines = executeCommande("zowe zos-files list data-set " + ds);
		boolean trouve = false;
		for (String line : lines) {
			System.out.println("LINE=" + line);
			if (line.contains(ds)) {
				trouve = true;
				break;
			}
		}
		System.out.println(trouve ? "" + ds + " exists" : "" + ds + " not exists yet");
		return trouve;
		/*
		 * String command = "zowe tso issue command \"" + "LISTCAT ENT('" + ds + "')\""; List<String> results = executeCommande(command);
		 * System.out.println("CHECK DS : " + ds + "   -> " + command); System.out.println("RES LIST = " + results.size()); for (String str : results) {
		 * System.out.println("   " + str); if (str.indexOf("NOT FOUND") > 0) return false; } return true;
		 */
	}
}
