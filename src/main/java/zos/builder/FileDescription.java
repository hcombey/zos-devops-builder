package zos.builder;

public class FileDescription implements Comparable<FileDescription> {
	String dsn;
	String dcb;

	public String getDsn() {
		return dsn;
	}

	public void setDsn(String dsn) {
		this.dsn = dsn;
	}

	public String getDcb() {
		return dcb;
	}

	public void setDcb(String dcb) {
		this.dcb = dcb;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dsn == null) ? 0 : dsn.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FileDescription other = (FileDescription) obj;
		if (dsn == null) {
			if (other.dsn != null)
				return false;
		} else if (!dsn.equals(other.dsn))
			return false;
		return true;
	}

	public FileDescription(String dsn, String dcb) {
		super();
		this.dsn = dsn;
		this.dcb = dcb;
	}

	@Override
	public int compareTo(FileDescription o) {
		if (this.equals(o)) {
			return 0;
		}
		return this.dsn.compareTo(o.dsn);
	}

}
