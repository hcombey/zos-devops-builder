package zos.builder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

class StreamGobbler2 implements Runnable {
	private InputStream inputStream;
	private boolean finished = false;
	private ArrayList<String> results = new ArrayList<>();

	public StreamGobbler2(InputStream inputStream) {
		this.inputStream = inputStream;
		this.finished = false;
	}

	@Override
	public void run() {

		try {
			BufferedReader buff = new BufferedReader(new InputStreamReader(inputStream));
			String str = buff.readLine();
			System.out.println("lecture de " + str);
			while (str != null) {
				results.add(str);
				System.out.println("lecture de " + str);
				str = buff.readLine();
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.finished = true;

	}

	public boolean isFinished() {
		return finished;
	}

	public ArrayList<String> getResults() {
		return results;
	}
}