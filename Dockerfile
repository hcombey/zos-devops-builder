FROM openjdk:11-jdk-stretch

ENV HOST_IP "xxxx"
ENV HOST_PORT "xxxxx"
ENV USER "xxxxx"
ENV PASSWORD "xxxxx"
ENV SECOND_HLQ ""

RUN apt-get update \
    && apt-get -y upgrade \
    && apt -y install curl dirmngr apt-transport-https lsb-release ca-certificates 

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash - \
    && apt-get install -y nodejs 
 
RUN mkdir /home/zowe \
	&& mkdir /home/project \
    && cd /home/zowe

RUN npm i -f --ignore-scripts -g  @zowe/cli

COPY target/zos-0.0.1-SNAPSHOT.jar /home/zowe/app.jar


ENTRYPOINT ["java","-jar","/home/zowe/app.jar"]
